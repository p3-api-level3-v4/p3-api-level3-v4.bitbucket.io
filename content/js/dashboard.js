/*
   Licensed to the Apache Software Foundation (ASF) under one or more
   contributor license agreements.  See the NOTICE file distributed with
   this work for additional information regarding copyright ownership.
   The ASF licenses this file to You under the Apache License, Version 2.0
   (the "License"); you may not use this file except in compliance with
   the License.  You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
var showControllersOnly = false;
var seriesFilter = "";
var filtersOnlySampleSeries = true;

/*
 * Add header in statistics table to group metrics by category
 * format
 *
 */
function summaryTableHeader(header) {
    var newRow = header.insertRow(-1);
    newRow.className = "tablesorter-no-sort";
    var cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 1;
    cell.innerHTML = "Requests";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 3;
    cell.innerHTML = "Executions";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 7;
    cell.innerHTML = "Response Times (ms)";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 1;
    cell.innerHTML = "Throughput";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 2;
    cell.innerHTML = "Network (KB/sec)";
    newRow.appendChild(cell);
}

/*
 * Populates the table identified by id parameter with the specified data and
 * format
 *
 */
function createTable(table, info, formatter, defaultSorts, seriesIndex, headerCreator) {
    var tableRef = table[0];

    // Create header and populate it with data.titles array
    var header = tableRef.createTHead();

    // Call callback is available
    if(headerCreator) {
        headerCreator(header);
    }

    var newRow = header.insertRow(-1);
    for (var index = 0; index < info.titles.length; index++) {
        var cell = document.createElement('th');
        cell.innerHTML = info.titles[index];
        newRow.appendChild(cell);
    }

    var tBody;

    // Create overall body if defined
    if(info.overall){
        tBody = document.createElement('tbody');
        tBody.className = "tablesorter-no-sort";
        tableRef.appendChild(tBody);
        var newRow = tBody.insertRow(-1);
        var data = info.overall.data;
        for(var index=0;index < data.length; index++){
            var cell = newRow.insertCell(-1);
            cell.innerHTML = formatter ? formatter(index, data[index]): data[index];
        }
    }

    // Create regular body
    tBody = document.createElement('tbody');
    tableRef.appendChild(tBody);

    var regexp;
    if(seriesFilter) {
        regexp = new RegExp(seriesFilter, 'i');
    }
    // Populate body with data.items array
    for(var index=0; index < info.items.length; index++){
        var item = info.items[index];
        if((!regexp || filtersOnlySampleSeries && !info.supportsControllersDiscrimination || regexp.test(item.data[seriesIndex]))
                &&
                (!showControllersOnly || !info.supportsControllersDiscrimination || item.isController)){
            if(item.data.length > 0) {
                var newRow = tBody.insertRow(-1);
                for(var col=0; col < item.data.length; col++){
                    var cell = newRow.insertCell(-1);
                    cell.innerHTML = formatter ? formatter(col, item.data[col]) : item.data[col];
                }
            }
        }
    }

    // Add support of columns sort
    table.tablesorter({sortList : defaultSorts});
}

$(document).ready(function() {

    // Customize table sorter default options
    $.extend( $.tablesorter.defaults, {
        theme: 'blue',
        cssInfoBlock: "tablesorter-no-sort",
        widthFixed: true,
        widgets: ['zebra']
    });

    var data = {"OkPercent": 93.2633103947845, "KoPercent": 6.736689605215502};
    var dataset = [
        {
            "label" : "FAIL",
            "data" : data.KoPercent,
            "color" : "#FF6347"
        },
        {
            "label" : "PASS",
            "data" : data.OkPercent,
            "color" : "#9ACD32"
        }];
    $.plot($("#flot-requests-summary"), dataset, {
        series : {
            pie : {
                show : true,
                radius : 1,
                label : {
                    show : true,
                    radius : 3 / 4,
                    formatter : function(label, series) {
                        return '<div style="font-size:8pt;text-align:center;padding:2px;color:white;">'
                            + label
                            + '<br/>'
                            + Math.round10(series.percent, -2)
                            + '%</div>';
                    },
                    background : {
                        opacity : 0.5,
                        color : '#000'
                    }
                }
            }
        },
        legend : {
            show : true
        }
    });

    // Creates APDEX table
    createTable($("#apdexTable"), {"supportsControllersDiscrimination": true, "overall": {"data": [0.13111191597247374, 500, 1500, "Total"], "isController": false}, "titles": ["Apdex", "T (Toleration threshold)", "F (Frustration threshold)", "Label"], "items": [{"data": [0.6371841155234657, 500, 1500, "getStaffHealthRecords"], "isController": false}, {"data": [0.0, 500, 1500, "getCentreHolidaysOfYear"], "isController": false}, {"data": [0.0, 500, 1500, "getPastChildrenData"], "isController": false}, {"data": [0.02097902097902098, 500, 1500, "suggestProgram"], "isController": false}, {"data": [0.2767857142857143, 500, 1500, "findAllWithdrawalDraft"], "isController": false}, {"data": [0.06343283582089553, 500, 1500, "findAllLevels"], "isController": false}, {"data": [0.017985611510791366, 500, 1500, "findAllClass"], "isController": false}, {"data": [0.07720588235294118, 500, 1500, "getChildrenToAssignToClass"], "isController": false}, {"data": [0.3484848484848485, 500, 1500, "getStaffCheckInOutRecords"], "isController": false}, {"data": [0.3418803418803419, 500, 1500, "getCountStaffCheckInOut"], "isController": false}, {"data": [0.0, 500, 1500, "searchBroadcastingScope"], "isController": false}, {"data": [0.3046875, 500, 1500, "getTransferDrafts"], "isController": false}, {"data": [0.0, 500, 1500, "leadByID"], "isController": false}, {"data": [0.0, 500, 1500, "registrationByID"], "isController": false}, {"data": [0.014705882352941176, 500, 1500, "getRegEnrolmentForm"], "isController": false}, {"data": [0.0, 500, 1500, "findAllLeads"], "isController": false}, {"data": [0.0, 500, 1500, "getEnrollmentPlansByYear"], "isController": false}, {"data": [0.021739130434782608, 500, 1500, "getAvailableVacancy"], "isController": false}, {"data": [0.0, 500, 1500, "getRegistrations"], "isController": false}]}, function(index, item){
        switch(index){
            case 0:
                item = item.toFixed(3);
                break;
            case 1:
            case 2:
                item = formatDuration(item);
                break;
        }
        return item;
    }, [[0, 0]], 3);

    // Create statistics table
    createTable($("#statisticsTable"), {"supportsControllersDiscrimination": true, "overall": {"data": ["Total", 2761, 186, 6.736689605215502, 156807.9934806228, 2, 602661, 87197.0, 420928.6000000001, 600003.0, 600040.38, 3.0554067752344953, 18.289729469652872, 6.22063473869411], "isController": false}, "titles": ["Label", "#Samples", "FAIL", "Error %", "Average", "Min", "Max", "Median", "90th pct", "95th pct", "99th pct", "Transactions/s", "Received", "Sent"], "items": [{"data": ["getStaffHealthRecords", 277, 0, 0.0, 6294.98555956679, 2, 176000, 136.0, 18234.000000000036, 33732.599999999955, 89321.37999999934, 0.7804685628953411, 0.45959232756434637, 0.9624223405604159], "isController": false}, {"data": ["getCentreHolidaysOfYear", 147, 0, 0.0, 187368.93197278903, 3526, 571342, 176318.0, 316225.4, 350478.19999999995, 548425.8400000004, 0.1790495029244752, 0.4759749272078509, 0.22993173471258294], "isController": false}, {"data": ["getPastChildrenData", 158, 28, 17.72151898734177, 433053.6835443038, 164647, 602303, 426824.5, 600011.1, 600048.2, 601164.3, 0.17976914456056114, 0.3242237944516188, 0.40992280522354524], "isController": false}, {"data": ["suggestProgram", 143, 0, 0.0, 94224.2937062937, 332, 471609, 72079.0, 217577.59999999998, 261397.59999999971, 440880.28000000014, 0.20162512672068714, 0.16342661638493197, 0.2502593125605404], "isController": false}, {"data": ["findAllWithdrawalDraft", 112, 0, 0.0, 14070.383928571428, 45, 245368, 3631.5, 31415.800000000003, 49179.29999999998, 241899.08000000013, 0.3462700226003024, 0.22419631346093796, 1.1304498882351668], "isController": false}, {"data": ["findAllLevels", 134, 0, 0.0, 54830.02985074629, 229, 316640, 38292.0, 109760.0, 185501.5, 314071.70000000007, 0.34245612600340924, 0.21804823647873323, 0.2869407774520753], "isController": false}, {"data": ["findAllClass", 139, 3, 2.158273381294964, 94443.28057553958, 669, 459106, 69310.0, 200813.0, 281424.0, 428308.39999999956, 0.1908690446111764, 18.61305210415957, 0.15100453416967846], "isController": false}, {"data": ["getChildrenToAssignToClass", 136, 0, 0.0, 42930.279411764706, 202, 506790, 29035.0, 88912.79999999997, 147638.85, 411710.3599999988, 0.19851406384562612, 0.1130211906464844, 0.14558990424615742], "isController": false}, {"data": ["getStaffCheckInOutRecords", 132, 0, 0.0, 8825.393939393942, 27, 232697, 2158.5, 18656.4, 35692.299999999974, 187589.95999999827, 0.36617030247886195, 0.2170560289108098, 0.47916816925944833], "isController": false}, {"data": ["getCountStaffCheckInOut", 117, 0, 0.0, 7878.384615384616, 21, 111184, 2499.0, 16540.2, 30099.3, 109870.17999999995, 0.28934755835175757, 0.1709524148464974, 0.29499887785081536], "isController": false}, {"data": ["searchBroadcastingScope", 147, 0, 0.0, 233129.8639455782, 85094, 530288, 209645.0, 380596.40000000026, 444881.79999999993, 521308.1600000002, 0.1827626249188758, 0.35417543440313703, 0.3582076056759606], "isController": false}, {"data": ["getTransferDrafts", 128, 0, 0.0, 20496.453125, 13, 488936, 3323.0, 54762.80000000002, 82816.39999999995, 406043.84999999823, 0.1867481744636461, 0.1200002917940226, 0.3240737363495109], "isController": false}, {"data": ["leadByID", 136, 1, 0.7352941176470589, 175170.61764705883, 7134, 600003, 151911.5, 318356.7, 353793.1000000001, 542836.1499999993, 0.16988044663568014, 0.2514734162083434, 0.7671163918392432], "isController": false}, {"data": ["registrationByID", 148, 2, 1.3513513513513513, 256886.96621621615, 13456, 600013, 247835.0, 404652.0, 430422.8499999999, 600011.53, 0.19498416412396777, 0.25620490991204636, 0.8869494496967206], "isController": false}, {"data": ["getRegEnrolmentForm", 136, 0, 0.0, 146128.8455882353, 701, 380210, 146059.0, 256512.19999999998, 335916.2500000001, 375712.64999999997, 0.3039459333822031, 0.5497605482927626, 1.2994875940891457], "isController": false}, {"data": ["findAllLeads", 145, 5, 3.4482758620689653, 279928.1793103449, 88086, 600024, 267961.0, 411086.00000000006, 519951.99999999924, 600019.86, 0.1762600772140697, 0.28096910449548285, 0.3989949794748179], "isController": false}, {"data": ["getEnrollmentPlansByYear", 144, 144, 100.0, 600052.090277778, 600000, 602661, 600011.0, 600051.5, 600126.0, 601931.55, 0.1595048261286628, 0.08489270531261839, 0.26464716757089657], "isController": false}, {"data": ["getAvailableVacancy", 138, 0, 0.0, 71000.55072463767, 176, 342866, 48205.0, 192998.0000000001, 254924.94999999995, 340282.6399999999, 0.22295103155241772, 0.12214407099697884, 0.29501821069680273], "isController": false}, {"data": ["getRegistrations", 144, 3, 2.0833333333333335, 252657.00694444444, 24205, 600024, 258489.0, 377625.5, 411379.5, 600019.05, 0.19210629881867966, 0.32391751560863674, 0.5932032391256495], "isController": false}]}, function(index, item){
        switch(index){
            // Errors pct
            case 3:
                item = item.toFixed(2) + '%';
                break;
            // Mean
            case 4:
            // Mean
            case 7:
            // Median
            case 8:
            // Percentile 1
            case 9:
            // Percentile 2
            case 10:
            // Percentile 3
            case 11:
            // Throughput
            case 12:
            // Kbytes/s
            case 13:
            // Sent Kbytes/s
                item = item.toFixed(2);
                break;
        }
        return item;
    }, [[0, 0]], 0, summaryTableHeader);

    // Create error table
    createTable($("#errorsTable"), {"supportsControllersDiscrimination": false, "titles": ["Type of error", "Number of errors", "% in errors", "% in all samples"], "items": [{"data": ["502/Bad Gateway", 183, 98.38709677419355, 6.628033321260413], "isController": false}, {"data": ["Non HTTP response code: javax.net.ssl.SSLException/Non HTTP response message: java.net.SocketException: Connection reset", 3, 1.6129032258064515, 0.10865628395508874], "isController": false}]}, function(index, item){
        switch(index){
            case 2:
            case 3:
                item = item.toFixed(2) + '%';
                break;
        }
        return item;
    }, [[1, 1]]);

        // Create top5 errors by sampler
    createTable($("#top5ErrorsBySamplerTable"), {"supportsControllersDiscrimination": false, "overall": {"data": ["Total", 2761, 186, "502/Bad Gateway", 183, "Non HTTP response code: javax.net.ssl.SSLException/Non HTTP response message: java.net.SocketException: Connection reset", 3, null, null, null, null, null, null], "isController": false}, "titles": ["Sample", "#Samples", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors"], "items": [{"data": [], "isController": false}, {"data": [], "isController": false}, {"data": ["getPastChildrenData", 158, 28, "502/Bad Gateway", 28, null, null, null, null, null, null, null, null], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": ["findAllClass", 139, 3, "Non HTTP response code: javax.net.ssl.SSLException/Non HTTP response message: java.net.SocketException: Connection reset", 3, null, null, null, null, null, null, null, null], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": ["leadByID", 136, 1, "502/Bad Gateway", 1, null, null, null, null, null, null, null, null], "isController": false}, {"data": ["registrationByID", 148, 2, "502/Bad Gateway", 2, null, null, null, null, null, null, null, null], "isController": false}, {"data": [], "isController": false}, {"data": ["findAllLeads", 145, 5, "502/Bad Gateway", 5, null, null, null, null, null, null, null, null], "isController": false}, {"data": ["getEnrollmentPlansByYear", 144, 144, "502/Bad Gateway", 144, null, null, null, null, null, null, null, null], "isController": false}, {"data": [], "isController": false}, {"data": ["getRegistrations", 144, 3, "502/Bad Gateway", 3, null, null, null, null, null, null, null, null], "isController": false}]}, function(index, item){
        return item;
    }, [[0, 0]], 0);

});
